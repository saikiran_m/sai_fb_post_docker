FROM python
USER root
ENV PYTHONBUFFERED 1
ENV DJANGO_SETTINGS_MODULE mysite.settings
RUN mkdir /code
WORKDIR /code
ADD requirements.txt /code/
ADD wait-for-mysql.sh /tmp/
ADD wait_for_mysql.py /tmp/
RUN chmod u=rwx /tmp/wait-for-mysql.sh
RUN chmod u=rwx /tmp/wait_for_mysql.py
RUN pip install -r requirements.txt
COPY . /code/
