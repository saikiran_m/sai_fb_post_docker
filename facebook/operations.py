from facebook.constants.reactions import ReactionType
from .models import User, Post, Comment, Reaction
# from collections import Counter
from django.db.models import Count, Q, F, Prefetch


class InvalidUserId(Exception):
    pass


class InvalidPostId(Exception):
    pass


class InvalidCommentId(Exception):
    pass


def get_post(post_id):
    validate_post_id(post_id)
    post = Post.objects.select_related('posted_user').prefetch_related('posted_user', 'reactions',
                                                                       'comments__commented_user',
                                                                       'comments__reactions').get(id=post_id)
    return convert_post_to_dictionary(post)


def convert_post_to_dictionary(post):
    comment_for_post = post.comments.all()
    post_dictionary = {
        "post_id": post.id,
        "posted_by": convert_user_to_dictionary(post.posted_user),
        "posted_at": post.post_time,
        "post_content": post.post_content,
        "reactions": get_reaction_type_and_reaction_count_for_given_reactions(post.reactions.all()),
        "comments": convert_given_comments_to_dict_along_replies(comment_for_post),
        "comment_count": len(comment_for_post)
    }
    return post_dictionary


def convert_given_comments_to_dict_along_replies(comments):
    comment_dictionaries = []
    list_to_check_if_replies_already_accessed = []
    for comment_obj in comments:
        comment_dictionary = convert_comment_to_dictionary(comment_obj)
        if comment_dictionary not in list_to_check_if_replies_already_accessed:
            reply_list = get_replies_for_given_comment(comment_obj, comments)
            list_to_check_if_replies_already_accessed.extend(reply_list)
            comment_dictionary.update({
                "replies_count": len(reply_list),
                "replies": reply_list
            })
            comment_dictionaries.append(comment_dictionary)
    return comment_dictionaries


# change the name of this method
def get_replies_for_given_comment(comment_obj, comments):
    reply_list = []
    for reply in comments:
        if comment_obj.id == reply.parent_comment_id:
            reply_list.append(convert_comment_to_dictionary(reply))
    return reply_list


def convert_comment_to_dictionary(comment):
    user = convert_user_to_dictionary(comment.commented_user)
    reactions = get_reaction_type_and_reaction_count_for_given_reactions(comment.reactions.all())
    return {
        "comment_id": comment.id,
        "commenter": user,
        "commented_at": comment.comment_time,
        "comment_content": comment.comment_content,
        "reactions": reactions
    }


def convert_user_to_dictionary(user):
    return {
        "user_id": user.id,
        "name": user.user_name,
        "profile_pic": user.profile_pic
    }


# TODO: change this method name to intentions revealing name
def get_reaction_type_and_reaction_count_for_given_reactions(reactions):
    reaction_type_list = []
    reaction_type_list.extend({reaction_obj.reaction_type for reaction_obj in reactions})
    return {
        "count": len(reactions),
        "type": reaction_type_list
    }


def create_post(user_id, post_content):
    user = validate_user_id(user_id)
    post = Post.objects.create(posted_user=user, post_content=post_content)
    return post.id


def add_comment(post_id, comment_user_id, comment_text):
    post = validate_post_id(post_id)
    user = validate_user_id(comment_user_id)
    comment = Comment.objects.create(post=post, commented_user=user, comment_content=comment_text)
    return comment.id


# TODO: must return the reply id and add parent-id to the created reply, use encapsulation conditions
def reply_to_comment(comment_id, reply_user_id, reply_text):
    comment = validate_comment_id(comment_id)
    user = validate_user_id(reply_user_id)
    parent_comment_for_new_comment = comment

    is_reply = bool(comment.parent_comment)
    if is_reply:
        parent_comment_for_new_comment = comment.parent_comment

    reply = Comment.objects.create(post=comment.post, commented_user=user, comment_content=reply_text,
                                   parent_comment=parent_comment_for_new_comment)
    return reply.id


# TODO: get only one post reaction object, change the name of the user_reaction_on_post
def react_to_post(user_id, post_id, reaction_type):
    user = validate_user_id(user_id)
    post = validate_post_id(post_id)
    user_reaction_on_post = Reaction.objects.filter(reacted_user__id=user_id, post__id=post_id)
    has_user_reacted = bool(user_reaction_on_post)

    if has_user_reacted:
        user_reaction_on_post = user_reaction_on_post[0]
        update_or_delete_reaction(user_reaction_on_post, reaction_type)

    else:
        Reaction.objects.create(reacted_user=user, reaction_type=reaction_type, post=post)


def update_or_delete_reaction(user_reaction_object, reaction_type):
    if user_reaction_object.reaction_type == reaction_type:
        user_reaction_object.delete()
    else:
        user_reaction_object.reaction_type = reaction_type
        user_reaction_object.save()


def react_to_comment(user_id, comment_id, reaction_type):
    user = validate_user_id(user_id)
    comment = validate_comment_id(comment_id)
    user_reaction_on_comment = Reaction.objects.filter(reacted_user__id=user_id, comment__id=comment_id)
    has_user_reacted = bool(user_reaction_on_comment)

    if has_user_reacted:
        user_reaction_on_comment = user_reaction_on_comment[0]
        update_or_delete_reaction(user_reaction_on_comment, reaction_type)

    else:
        Reaction.objects.create(reacted_user=user, reaction_type=reaction_type, comment=comment)


def get_user_posts(user_id):
    total_posts_list = []
    validate_user_id(user_id)
    user = User.objects.prefetch_related('posts').get(id=user_id)  # TODO: handle the errors here
    total_posts_by_user = user.posts.prefetch_related('reactions', 'comments__commented_user',
                                                      'comments__reactions').all()
    for user_post in total_posts_by_user:
        post_dictionary = convert_post_to_dictionary(user_post)
        total_posts_list.append(post_dictionary)
    return total_posts_list


def get_posts_with_more_positive_reactions():
    positive_reactions = [ReactionType.LIKE.value, ReactionType.LOVE.value, ReactionType.HAHA.value,
                          ReactionType.WOW.value]
    negative_reactions = [ReactionType.SAD.value, ReactionType.ANGRY.value]
    positive_reactions_on_post = Count('reactions', filter=Q(reactions__reaction_type__in=positive_reactions))
    negative_reactions_on_post = Count('reactions', filter=Q(reactions__reaction_type__in=negative_reactions))
    posts = Post.objects.annotate(positive=positive_reactions_on_post, negative=negative_reactions_on_post).filter(
        positive__gt=F('negative')
    )
    return [post.id for post in posts]


def get_posts_reacted_by_user(user_id):
    user = validate_user_id(user_id)
    user_reaction_on_posts = Reaction.objects.filter(reacted_user__id=user.id, comment=None)
    post_ids_list = [reaction.post_id for reaction in user_reaction_on_posts]
    return post_ids_list


# TODO: change the query to get the reactions
def get_reactions_to_post(post_id):
    validate_post_id(post_id)

    reactions_to_post_list = []
    reactions_to_post = Reaction.objects.filter(post__id=post_id)

    for reaction_obj in reactions_to_post:
        user = reaction_obj.reacted_user
        user_dictionary = convert_user_to_dictionary(user)
        user_dictionary["reaction"] = reaction_obj.reaction_type

        reactions_to_post_list.append(user_dictionary)

    return reactions_to_post_list


# TODO: change the query, remove post objects and use post_id
def get_reaction_metrics(post_id):
    validate_post_id(post_id)
    reactions_metrics_list = Reaction.objects.filter(post__id=post_id, comment=None).values(
        'reaction_type').annotate(
        count=Count('reaction_type'))
    return reactions_metrics_list


def get_total_reaction_count():
    post_reactions = Reaction.objects.filter(comment=None)
    return post_reactions.count()


# TODO- change the query to get the replies
def get_replies_for_comment(comment_id):
    validate_comment_id(comment_id)
    replies = Comment.objects.filter(parent_comment=comment_id)
    replies_list = []
    for reply_obj in replies:
        comment_dictionary = convert_comment_to_dictionary(reply_obj)
        comment_dictionary.pop('reactions')
        replies_list.append(comment_dictionary)
    return replies_list


def delete_post(post_id):
    post = validate_post_id(post_id)
    post.delete()


def validate_user_id(user_id):
    try:
        return User.objects.get(id=user_id)
    except User.DoesNotExist:
        raise InvalidUserId


def validate_post_id(post_id):
    try:
        return Post.objects.get(id=post_id)
    except Post.DoesNotExist:
        raise InvalidPostId


def validate_comment_id(comment_id):
    try:
        return Comment.objects.prefetch_related('reactions', 'commented_user').get(id=comment_id)
    except Comment.DoesNotExist:
        raise InvalidCommentId
