# from dataclasses import dataclass, make_dataclass
# from collections import namedtuple
# import attr
# from typing import *


# def trace(func):
#     print("entered into {} block ".format(func.__name__))
#
#     def ret_func(*args):
#         print(func.__name__, args)
#         if func.__name__ is "square":
#             val = args[0]-1
#             return func(val)
#         return func(*args)
#
#     return ret_func
#
#
# @trace
# def fib(n):
#     if n == 0 or n == 1:
#         return 1
#     else:
#         return fib(n-1) + fib(n-2)
#
#
# @trace
# def square(x):
#     return x * x
#
#
# @trace
# def sum_of_squares(x, y):
#     return square(x) + square(y)
#
#
# print(square(4))

#
# nums = [1, 2, 3, 4, 5]
#
# i_nums = iter(nums)
#
# while 1:
#     try:
#         val = next(i_nums)
#         print(val)
#     except StopIteration:
#         break
#
#
# print(dir(i_nums))
# # for num in nums:
#     print(num)

# class MyRange:
#
#     def __init__(self, start, end):
#         self.start_val = start
#         self.end_val = end
#
#     def __iter__(self):
#         return self
#
#     def __next__(self):
#
#         if self.start_val >= self.end_val:
#             raise StopIteration
#
#         current_val = self.start_val
#         self.start_val += 1
#         return current_val
#
#
# nums = MyRange(start=1, end=10)
#
# for num in nums:
#     print(num)


# def my_generator(start, end):
#
#     while start < end:
#         yield start
#         start += 1
#
#
# nums = my_generator(start=1, end=10)
#
# for num in nums:
#     print(num)
#
#

import datetime
from django.utils import timezone
import pytz

tz = pytz.timezone()

#
#
#
# def fib(n: int) -> Generator:
#     a: int = 0
#     b: int = 1
#     for _ in range(n):
#         yield a
#         b, a = a + b, b
#
#
# # print([n for n in fib(3)])
#
# # var_could_be_none: Optional[str] = None
# # var_could_be_none = '1121asa'
# # print(var_could_be_none)
#
# class Foo:
#
#     x: int = 1  # instance variable. default = 1
#     y: ClassVar[str] = "class var"  # class variable
#
#     def __init__(self) -> None:
#         self.i: List[int] = [0]
#
#     def foo(self, a: int, b: str) -> Dict[int, str]:
#         return {a: b}
#
#
# foo = Foo()
# foo.x = 123
#
# print(foo.x)
# print(type(foo.i))
# print(Foo.y)
#
#
# def estimate_time_function(func: Callable[[int, int], Any]) -> [Any, float]:
#     import time
#     a: float
#     a = time.time()
#     result: Any
#     result = func(1, 2)
#     b: float
#     b = time.time()
#     return result, b-a
#
#
#
#
#
#
#
#
#
#
#
# #
# # @dataclass
# # class PlayingCard:
# #     rank: str
# #     suit: str
# #
# #
# # """Alternatives to the dataclasses 1)namedtuple 2)attrs"""
# # NamedTupleCard = namedtuple("NamedTupleCard", ["rank", "suit"])
# #
# #
# # @attr.s
# # class AttrsCard:
# #     rank = attr.ib()
# #     suit = attr.ib()
# #
# #
# # @dataclass
# # class Position:
# #     name: str
# #     lat: float  # = 0.0 default value
# #     lon: float  # Any
# #
# #
# # PositionClass = make_dataclass("PositionClass", ["name", "lat", "lon"])
