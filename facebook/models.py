from django.contrib.auth.models import AbstractUser
from django.db import models
from datetime import datetime
from facebook.constants.reactions import ReactionType


# Create your models here.


class Post(models.Model):
    posted_user = models.ForeignKey('User', on_delete=models.CASCADE, related_name='posts')
    post_content = models.TextField()
    post_time = models.DateTimeField(default=datetime.now)

    def __str__(self):
        return self.post_content


class User(AbstractUser):
    user_name = models.CharField(max_length=20)
    profile_pic = models.URLField(null=True, blank=True)

    def __str__(self):
        return self.user_name


class Reaction(models.Model):
    REACTIONS = (
        (ReactionType.LIKE.value, ReactionType.LIKE.value),
        (ReactionType.LOVE.value, ReactionType.LOVE.value),
        (ReactionType.SAD.value, ReactionType.SAD.value),
        (ReactionType.WOW.value, ReactionType.WOW.value),
        (ReactionType.ANGRY.value, ReactionType.ANGRY.value),
        (ReactionType.HAHA.value, ReactionType.HAHA.value),
    )
    reacted_user = models.ForeignKey('User', on_delete=models.CASCADE, related_name='reactions')
    reaction_type = models.CharField(max_length=10, choices=REACTIONS)
    post = models.ForeignKey(Post, on_delete=models.CASCADE, null=True, blank=True, related_name='reactions')
    comment = models.ForeignKey('Comment', on_delete=models.CASCADE, null=True, blank=True, related_name='reactions')

    class Meta:
        unique_together = [['post', 'reacted_user'], ['comment', 'reacted_user']]

    def __str__(self):
        return self.reaction_type


class Comment(models.Model):

    commented_user = models.ForeignKey('User', on_delete=models.CASCADE, related_name='comments')
    post = models.ForeignKey('Post', on_delete=models.CASCADE, related_name='comments')
    comment_content = models.CharField(max_length=100)
    parent_comment = models.ForeignKey('self', null=True, blank=True, on_delete=models.CASCADE, related_name='replies')
    comment_time = models.DateTimeField(default=datetime.now)

    def __str__(self):
        return self.comment_content

