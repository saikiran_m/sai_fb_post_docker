from django.contrib import admin

from .models import User, Reaction, Post, Comment
# Register your models here.

@admin.register(Post)
class Posts(admin.ModelAdmin):
    list_display = ('post_content', 'post_time')

admin.site.register(User)
admin.site.register(Reaction)
# admin.site.register(Post)
admin.site.register(Comment)