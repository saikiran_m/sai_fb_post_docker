from facebook.models import *
from facebook.operations import *
import pytest


@pytest.mark.django_db
def test_check_if_valid_post_id():
    with pytest.raises(InvalidPostId):
        get_post(1213)


@pytest.mark.django_db
def test_check_attribute_values_returned_by_get_post(check_attribute_values_of_get_post, create_post,
                                                     create_comment_for_given_post,
                                                     create_reaction_for_given_post, create_reply_for_comment):
    post = create_post
    comment = create_comment_for_given_post(post.id)
    create_reply_for_comment(comment.id)
    create_reaction_for_given_post(post.id)
    post_dict = get_post(post.id)

    status = check_attribute_values_of_get_post(post_dict, post)

    assert status
