from facebook.models import *
from facebook.operations import create_post, InvalidUserId
import pytest


@pytest.mark.django_db
def test_create_post(create_user):
    user = create_user('sai')
    create_post(user.id, "new post")
    posts = Post.objects.all()
    assert len(posts) == 1


@pytest.mark.django_db
def test_user_id():
    with pytest.raises(InvalidUserId):
        create_post(20, "new post")
