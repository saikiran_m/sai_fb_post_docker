from facebook.models import *
from facebook.operations import *
import pytest


@pytest.mark.django_db
def test_valid_post_id():
    with pytest.raises(InvalidPostId):
        get_reaction_metrics(1312)


@pytest.mark.django_db
def test_number_of_reaction_types_for_post(add_n_no_of_reactions_to_single_post_to_get_reaction_metrics, create_post, get_count_of_reactions_from_reaction_metrics):
    post = create_post
    add_n_no_of_reactions_to_single_post_to_get_reaction_metrics(post.id)
    reaction_metrics = get_reaction_metrics(post.id)

    reaction_count_for_sad = get_count_of_reactions_from_reaction_metrics(reaction_metrics)

    assert reaction_count_for_sad == 2