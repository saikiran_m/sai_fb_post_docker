from facebook.operations import *
import pytest


@pytest.mark.django_db
def test_valid_user_id():
    with pytest.raises(InvalidUserId):
        get_user_posts(1431)


@pytest.mark.django_db
def test_check_user_posts_returned_by_get_user_posts_method(create_user, create_list_of_n_posts_given_user_id, check_attribute_values_for_list_of_user_posts):
    user = create_user('john')
    post_list = create_list_of_n_posts_given_user_id(4, user.id)
    post_dict_list = get_user_posts(user.id)
    no_of_posts_valid = check_attribute_values_for_list_of_user_posts(post_list, post_dict_list)
    assert no_of_posts_valid == len(post_list)