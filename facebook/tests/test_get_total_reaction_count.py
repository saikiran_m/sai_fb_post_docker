from facebook.models import *
from facebook.operations import *
import pytest


@pytest.mark.django_db
def test_reaction_count_on_all_posts(add_n_reactions_to_list_of_posts):
    add_n_reactions_to_list_of_posts(4)

    reactions = get_total_reaction_count()
    assert reactions == 4
