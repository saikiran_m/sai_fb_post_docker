from facebook.models import *
from facebook.operations import *
import pytest


@pytest.mark.django_db
def test_check_valid_post():
    with pytest.raises(InvalidPostId):
        delete_post(121)


@pytest.mark.django_db
def test_delete_post(create_post):
    post = create_post

    delete_post(post.id)

    posts = Post.objects.all()

    assert len(posts) == 0

