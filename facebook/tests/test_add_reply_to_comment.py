from facebook.models import *
from facebook.operations import *
import pytest


@pytest.mark.django_db
def test_check_valid_comment(create_user):
    user = create_user('sai')
    with pytest.raises(InvalidCommentId):
        reply_to_comment(121, user.id, "new reply to a comment")


@pytest.mark.django_db
def test_check_valid_user(create_comment):
    comment = create_comment
    with pytest.raises(InvalidUserId):
        reply_to_comment(comment.id, 212, " new reply to a comment")


@pytest.mark.django_db
def test_check_if_reply_added_to_comment(create_user, create_comment):
    user = create_user('sai')
    comment = create_comment

    reply_to_comment(comment.id, user.id, "new reply to this comment")
    replies = Comment.objects.filter(parent_comment=comment)

    assert len(replies) == 1


@pytest.mark.django_db
def test_check_reply_to_reply_is_added_as_reply_to_comment(create_user, create_comment):
    user = create_user('sai')
    comment = create_comment

    first_reply_id = reply_to_comment(comment.id, user.id, "first reply to a comment")
    first_reply = Comment.objects.get(id=first_reply_id)
    reply_to_comment(first_reply.id, user.id, "second reply to a comment")
    replies = Comment.objects.filter(parent_comment=comment.id)

    assert len(replies) == 2
