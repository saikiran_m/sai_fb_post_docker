from facebook.models import *
from facebook.operations import *
import pytest


@pytest.mark.django_db
def test_valid_user_id():
    with pytest.raises(InvalidUserId):
        get_posts_reacted_by_user(123)


@pytest.mark.django_db
def test_posts_count_reacted_by_user(add_n_reactions_to_list_of_posts_given_user, create_user):
    adam = create_user('adam')
    john = create_user('john')
    add_n_reactions_to_list_of_posts_given_user(5, john.id)

    add_n_reactions_to_list_of_posts_given_user(2, adam.id)

    posts = get_posts_reacted_by_user(adam.id)

    assert len(posts) == 2
