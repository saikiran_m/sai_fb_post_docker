from facebook.models import *
from facebook.operations import *
import pytest


@pytest.mark.django_db
def test_valid_post_id():
    with pytest.raises(InvalidPostId):
        get_reactions_to_post(123)


@pytest.mark.django_db
def test_number_of_reactions_to_post(create_post, create_user):
    post = create_post
    user = create_user('john')
    Reaction.objects.create(reacted_user=user, reaction_type='LIKE', post=post)

    reactions = get_reactions_to_post(post.id)

    assert len(reactions) == 1