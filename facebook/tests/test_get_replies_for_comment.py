from facebook.models import *
from facebook.operations import *
import pytest


@pytest.mark.django_db
def test_check_valid_comment_id():
    with pytest.raises(InvalidCommentId):
        get_replies_for_comment(121)


@pytest.mark.django_db
def test_number_of_replies_for_comment(create_comment, create_reply_for_comment, create_post, create_user):
    first_comment = create_comment
    create_reply_for_comment(first_comment.id)

    commented_user = create_user('john')

    second_comment = Comment.objects.create(post=create_post, commented_user=commented_user, comment_content="second comment")
    create_reply_for_comment(second_comment.id)
    create_reply_for_comment(second_comment.id)

    replies = get_replies_for_comment(second_comment.id)

    assert len(replies) == 2

