import datetime

import pytz
from django.utils.timezone import activate


class TimeZoneMiddleware:

    def __init__(self, get_response):
        self.get_response = get_response

    def process_request(self, request):

        activate(pytz.timezone('America/New_York'))

        print("current time is", datetime.datetime.now())

    def __call__(self, request):
        response = self.get_response(request)
        req = request.POST
        activate(pytz.timezone('Asia/Kolkata'))
        print("current time is", datetime.datetime.now())

        return response


